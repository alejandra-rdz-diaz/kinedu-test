class ApiController < ApplicationController
  
  #Vista default cuando se solicita un formato distinto a json
  rescue_from ActionView::MissingTemplate do |exception| 
    render json: {errors: ["Formato de peticion desconocido. Solo se admite formato json."]}, status: 400
  end

	rescue_from ActiveRecord::RecordNotFound do |exception| 
		render json: {errors: ["Elemento no encontrado"]}, status: 404
	end

end
