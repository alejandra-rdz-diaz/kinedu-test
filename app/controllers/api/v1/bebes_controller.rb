class Api::V1::BebesController < ApiController
  before_action :bebe_params, only: [:create, :update]
  before_action :set_bebe, only: [:show, :update, :destroy]

  def index  
    if params[:id_usuario].present?
      user = Usuario.find_by(id:params[:id_usuario])

      if user.nil?
        error = "Usuario id #{params[:id_usuario]} no fue encontrado."
      elsif user.familia.nil?
        error = "Usuario #{params[:id_usuario]} no esta relacionado con alguna familia."
      end

      if error.present?
        render json: { errors: [error] }, status: 404
      else
        @bebes = user.familia.bebes
      end
 
    else
      @bebes = Bebe.all
    end

  end

  def create
    new_params = bebe_params
    @bebe = Bebe.new(nombre: bebe_params[:nombre],  fecha_nacimiento: bebe_params[:fecha_nacimiento], 
                     sexo: bebe_params[:sexo] , familia_id: bebe_params[:id_familia] , usuario_creacion: bebe_params[:id_usuario])

    if @bebe.save
      render :show
    else
      render json: { errors: @bebe.errors.full_messages }, status: 404
    end
    
  end

  def show
  end

  def update
    if @bebe.update(nombre: bebe_params[:nombre],  fecha_nacimiento: bebe_params[:fecha_nacimiento], 
                     sexo: bebe_params[:sexo] , familia_id: bebe_params[:id_familia] , usuario_mod: bebe_params[:id_usuario])
      render :show
    else
      render json: { errors: @bebe.errors.full_messages }, status: 404
    end
  end

  def destroy
    if @bebe.destroy
      render :show
    else
      render json: { errors: @bebe.errors.full_messages }, status: 404
    end
  end

  def find_by_user
    if params[:id_usuario].nil?
      render json: { errors: ["Falta parametro id_usuario"] }, status: 400
    else
      redirect_to action: :index, params: params
    end    
  end

  private

  def bebe_params
    params.permit(:nombre, :fecha_nacimiento, :sexo, :id_usuario, :id_familia)
  end

  def set_bebe
    @bebe = Bebe.find_by(id: params[:id])

    if @bebe.nil?
      render json: { errors: ["Bebe id #{params[:id]} no fue encontrado"] }, status: 404 and return
    end
  end


end
