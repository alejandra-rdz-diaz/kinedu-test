class Api::V1::FamiliasController < ApiController
  before_action :familia_params, only: [:create, :update]
  before_action :set_familia, only: [:show, :update, :destroy]

  def index
    @familias = Familia.all
  end

  def create
    new_params = familia_params
    @familia = Familia.new(nombre: familia_params[:nombre], premium: familia_params[:premium] , usuario_creacion: familia_params[:id_usuario])

    if @familia.save
      render :show
    else
      render json: { errors: @familia.errors.full_messages }, status: 404
    end
  end

  def show
  end

  def update
    if @familia.update(nombre: familia_params[:nombre], premium: familia_params[:premium], usuario_mod: familia_params[:id_usuario])
      render :show
    else
      render json: { errors: @familia.errors.full_messages }, status: 404
    end
  end

  def destroy
    if @familia.destroy
      render :show
    else
      render json: { errors: @familia.errors.full_messages }, status: 404
    end
  end

  private

  def familia_params
    params.permit(:nombre, :premium, :id_usuario)
  end

  def set_familia
    @familia = Familia.find_by(id: params[:id])

    if @familia.nil?
      render json: { errors: ["Familia id #{params[:id]} no fue encontrado"] }, status: 404 and return
    end
  end



end