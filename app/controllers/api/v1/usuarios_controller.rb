class Api::V1::UsuariosController < ApiController
  before_action :usuario_params, only: [:create, :update]
  before_action :set_usuario, only: [:show, :update, :destroy]

  def index
    @usuarios = Usuario.all
  end

  def create
    @usuario = Usuario.new(usuario_params)

    if @usuario.save
      render :show
    else
      render json: { errors: @usuario.errors.full_messages }, status: 404
    end
  end

  def show
  end

  def update
    if @usuario.update(usuario_params)
      render :show
    else
      render json: { errors: @usuario.errors.full_messages }, status: 404
    end
  end

  private

  def usuario_params
    params.permit(:nombre, :email)
  end

  def set_usuario
    @usuario = Usuario.find_by(id: (params[:id] || params[:id_usuario]))

    if @usuario.nil?
      render json: { errors: ["Usuario id #{params[:id]} no fue encontrado"] }, status: 404 and return
    end
  end

end
