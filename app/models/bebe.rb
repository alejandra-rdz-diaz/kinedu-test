class Bebe < ActiveRecord::Base
	self.table_name = "bebe"

	#Assoc
	belongs_to :familia, class_name: "Familia", foreign_key: "familia_id"


	#Validations
	validates :nombre, presence: {message: "es requerido"}

  validates :fecha_nacimiento, presence: {message: "es requerido"}

  validates :sexo, inclusion: { in: %w(F M), message: "'%{value}' no es un válido. Permitido: 'F' o 'M'" }, allow_nil: false

  validates :familia_id, presence: { message: "es requerido"}

 	validate :family_exists
 	validate :premium_family

  def family_exists
    errors.add(:familia_id, "no es válido") unless Familia.exists? self.familia_id
  end

  def premium_family
  	family = Familia.find_by(id: self.familia_id)
  	unless family.present? && (family.bebes.length < 2 || family.premium)
    	errors.add(:base, "No se permite agregar más bebes") 
  	end
  end

 #actions
 before_save :set_default_dates

 def set_default_dates
 	if self.new_record?
 		self.fecha_creacion ||= DateTime.now
 		self.fecha_mod ||= DateTime.now
 	else
 		self.fecha_mod = DateTime.now
 	end
 end


end
