class Familia < ActiveRecord::Base
	self.table_name = "familia"

	#assoc
	has_many :bebes, class_name: "Bebe", dependent: :destroy
	has_many :usuarios, class_name: "Usuario"

	#Validations
	validates :nombre, presence: {message: "es requerido"}

	#actions
 	before_save :set_default_dates
 	before_destroy :remove_users

	def set_default_dates
	 	if self.new_record?
	 		self.fecha_creacion ||= DateTime.now
	 		self.fecha_mod ||= DateTime.now
	 	else
	 		self.fecha_mod = DateTime.now
	 	end
	end

	def remove_users
		Usuario.where(familia_id: self.id).update_all(familia_id: nil)
	end

end
