class Usuario < ActiveRecord::Base
	self.table_name = "usuario"

	#assoc
	belongs_to :familia, class_name: "Familia", foreign_key: "familia_id"

	#Validations
	validates :nombre, presence: {message: "es requerido"}

	validates :email, presence: {message: "es requerido"}, uniqueness: true

	validate :email_format

	def email_format
		errors.add(:email, "formato inválido")  if (self.email =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/).nil?
	end


	#actions
	before_save :set_default_dates

	def set_default_dates
	 	if self.new_record?
	 		self.fecha_creacion ||= DateTime.now
	 		self.fecha_mod ||= DateTime.now
	 	else
	 		self.fecha_mod = DateTime.now
	 	end
	end

end
