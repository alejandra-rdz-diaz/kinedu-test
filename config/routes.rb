Rails.application.routes.draw do

  #Uso de namespace para separar api por version
  namespace :api, path: '/', defaults: {format: :json} do

    namespace :v1 do

      #Endpoints requeridos
      post "crearBebe", to: "bebes#create"
      get "getBebesByIdUsuario", to: "bebes#find_by_user"
      put "editUsuario", to: "usuarios#update"

      #Endpoints estilo REST
      resources :bebes
      resources :familias
      resources :usuarios

    end
  end


end
