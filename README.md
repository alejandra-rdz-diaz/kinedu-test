## Prueba kinedu

Api de conexion a BD kinedu, con namespace de version y endpoints adicionales con formato REST. El formato default de respuesta es json.

**API HOST:** 
http://localhost:3000/v1

**Enpoints requeridos**
Estos corresponden a los solicitados en la prueba 

*  ***getBebesByIdUsuario***
    * GET /getBebesByIdUsuario?id_usuario=1
    * params: id_usuario

*  ***crearBebe***
    * POST /crearBebe
    * params: nombre, fecha_nacimiento, sexo, id_familia, id_usuario


*  ***editUsuario***
    * PUT /editUsuario
    * params: nombre, email, id_usuario

**Endpoints REST**

* ***Bebe***
    * GET /bebes 
        * params: id_usuario(opcional)
        
    * GET /bebes/:id
    
    * POST /bebes
        * params: nombre, fecha_nacimiento, sexo, id_familia, id_usuario
 
    * PUT /bebes/:id
        * params: nombre, fecha_nacimiento, sexo, id_familia, id_usuario
 
    * DELETE /bebes/:id 
    
   
    
* ***Familia***
    * GET /familias
    
    * GET /familias/:id
    
    * POST /familias
        * params: nombre, premium, id_usuario
       
    * PUT /familias/:id
        * params: nombre, premium, id_usuario
       
    * DELETE /familias/:id 
 
 
* ***Usuario***
    * GET /usuarios
    
    * GET /usuarios/:id
    
    * POST /usuarios
        * params:  nombre, email, id_usuario
        
    * PUT /usuarios/:id
        *  params: nombre, email, id_usuario
 

#### Formato de respuesta error

HTTP status: 400, 404, 500

```
{
    errors: ["mensaje de error"]
}
```


